#!/bin/bash

sudo apt install -y python3-pip python-is-python3
sudo apt install -y cmake swig doxygen pkgconf libliquid-dev

git clone https://github.com/rpp0/gr-lora.git
cd gr-lora && mkdir build && cd build 

cmake ../
make

sudo make install
cd /usr/local/lib
sudo ln -s python3/dist-packages/lora python3.8/dist-packages/lora
cd ~
sudo ldconfig