#!/bin/bash

sudo apt install -y libpth20 libfftw3-dev libpcap-dev

# configure swap for compilation
sudo fallocate -l 4G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile

sudo pip install pybombs mako requests numpy

sudo pybombs auto-config
sudo pybombs recipes add-defaults

sudo mkdir /usr/local/gr
sudo pybombs prefix init /usr/local/gr -a gr
sudo pybombs config defaul_prefix /usr/local/gr

sudo pybombs install gnuradio
