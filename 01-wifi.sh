# copy wifi configuration

echo "sdr" |sudo tee /etc/hostname

sudo apt install -y avahi-daemon

sudo cp config/01-wifis.yaml /etc/netplan/01-wifis.yaml
sudo netplan apply

sudo systemctl restart systemd-networkd.service

ip a